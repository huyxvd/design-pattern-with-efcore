﻿AppDbContext dbContext = new AppDbContext();


IStudentRepository _studentRepository = new StudentRepository(dbContext);

// Fake dữ liệu
Student student = new Student
{
    FullName = "AAAA",
    Addresss = new[]
    {
        new Address { Detail = "Address of AAAA 1"},
        new Address { Detail = "Address of AAAA 2"}
    }
};

_studentRepository.AddNew(student);

List<Student> students = _studentRepository.LoadStudentWithAddress();

Console.WriteLine("-----------------");

IAddressRepository _addressRepository = new AddressRepository(dbContext);


List<Address> addresses = _addressRepository.LoadAddressIncludeStudent();

Console.WriteLine();
// Mặc dù cả 2 IStudentRepository và IAddressRepository đều có hàm AddNew & GetAll nhưng
// IStudentRepository có .LoadStudentWithAddress();
// IAddressRepository có .LoadAddressIncludeStudent();