﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<Student> Students { get; set; }
    public DbSet<Car> Cars { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseInMemoryDatabase("hi");
    }
}

public class Student
{
    public int Id { get; set; }
    public string FullName { get; set; }
}

public class Car
{
    public int Id { get; set; }
    public string Make { get; set; }
}

