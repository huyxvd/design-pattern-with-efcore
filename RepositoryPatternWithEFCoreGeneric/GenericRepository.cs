﻿using Microsoft.EntityFrameworkCore;

namespace RepositoryPatternWithEFCoreGeneric;

public interface IGenericRepository<T> where T : class
{
    public List<T> GetAll();
    public void AddNew(T item);
}

public class GenericRepository<T> : IGenericRepository<T> where T : class
{
    private readonly AppDbContext _dbContext; 
    private readonly DbSet<T> _dbSet; 
    public GenericRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
        _dbSet = _dbContext.Set<T>(); // gán cho db set kiểu T
    }
    public void AddNew(T item)
    {
        _dbSet.Add(item);
        _dbContext.SaveChanges();
    }

    public List<T> GetAll()
    {

        return _dbSet.ToList();
    }
}