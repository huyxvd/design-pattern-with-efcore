﻿using RepositoryPatternWithEFCoreGeneric;

AppDbContext dbContext = new AppDbContext();


IGenericRepository<Student> _studentGenericRepository = new GenericRepository<Student>(dbContext);

_studentGenericRepository.AddNew(new Student { FullName = "Student 1" });
_studentGenericRepository.AddNew(new Student { FullName = "Student 2" });
List<Student> students = _studentGenericRepository.GetAll();

foreach (Student student in students)
{
    Console.WriteLine(student.FullName);
}

Console.WriteLine("-----------------");

IGenericRepository<Car> _carGenericRepository = new GenericRepository<Car>(dbContext);

_carGenericRepository.AddNew(new Car { Make = "Honda" });
_carGenericRepository.AddNew(new Car { Make = "BMW" });
List<Car> cars = _carGenericRepository.GetAll();

foreach (Car car in cars)
{
    Console.WriteLine(car.Make);
}