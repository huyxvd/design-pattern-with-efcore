﻿namespace RepositoryPatternWithEFCore;

public class BusinessLogic
{
    private readonly IRepository _repository;
    public BusinessLogic(IRepository repository)
    {
        _repository = repository;
    }

    public void InsertStudent()
    {
        Student s1 = new Student
        {
            FullName = "test 11111"
        };

        Student s2 = new Student
        {
            FullName = "test 22222"
        };


        _repository.AddNewStudent(s1);
        _repository.AddNewStudent(s2);

        
    }

    public void GetAll()
    {
        List<Student> students = _repository.GetAllStudent();

        // Giả sử tính toán gì đó...
    }
}