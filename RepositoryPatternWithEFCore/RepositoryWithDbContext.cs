﻿namespace RepositoryPatternWithEFCore;

public class RepositoryWithDbContext : IRepository
{
    private readonly AppDbContext _context;
    public RepositoryWithDbContext(AppDbContext appContext)
    {
        _context = appContext;
    }

    public void AddNewStudent(Student student)
    {
        _context.Students.Add(student);
        _context.SaveChanges();
        Console.WriteLine($"Đã ghi {student.FullName} vào database");
    }

    public List<Student> GetAllStudent()
    {
        Console.WriteLine("Đã lấy dữ liêu từ database");
        return _context.Students.ToList();
    }
}
