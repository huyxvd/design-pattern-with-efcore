﻿using RepositoryPatternWithEFCore;

Console.OutputEncoding = System.Text.Encoding.UTF8;


IRepository _repository = new RepositoryWithDbContext(new AppDbContext());
BusinessLogic logic = new BusinessLogic(_repository); // Sử dụng dbcontext để lưu

Console.WriteLine("-- Sử dụng database để lưu --");
logic.InsertStudent();
logic.GetAll();

IRepository _repositoryLocalFile = new RepositoryFileLocal();
BusinessLogic logic2 = new BusinessLogic(_repositoryLocalFile); // Sử dụng file để lưu

Console.WriteLine("-- Sử dụng file để lưu --");
logic2.InsertStudent();
logic2.GetAll();

