﻿namespace RepositoryPatternWithEFCore;

public class RepositoryFileLocal : IRepository
{
    public RepositoryFileLocal()
    {
    }

    public void AddNewStudent(Student student)
    {
        Console.WriteLine($"Đã ghi {student.FullName} vào file thành công");
    }

    public List<Student> GetAllStudent()
    {
        Console.WriteLine("Đã lấy dữ liêu từ file");
        return new List<Student>();
    }
}
