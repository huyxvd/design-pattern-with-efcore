﻿namespace RepositoryPatternWithEFCore;

public interface IRepository
{
    public List<Student> GetAllStudent();
    public void AddNewStudent(Student student);
}
