﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<Student> Students { get; set; }
    public DbSet<Car> Cars { get; set; }
    public DbSet<Address> Addresss { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseInMemoryDatabase("hi");
    }
}


public class Student
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public ICollection<Address> Addresss { get; set; }
}

public class Address
{
    public int Id { get; set; }
    public int StudentId { get; set; }
    public Student Student { get; set; }
    public string Detail { get; set; }
}

public class Car
{
    public int Id { get; set; }
    public string Make { get; set; }
}