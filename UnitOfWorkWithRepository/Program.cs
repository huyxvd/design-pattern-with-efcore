﻿using Microsoft.EntityFrameworkCore;

AppDbContext context = new AppDbContext();
IUnitOfWork unitOfWork = new UnitOfWork(context);

Student student = new Student
{
    FullName = "AAAA",
    Addresss = new[]
    {
        new Address { Detail = "Address of AAAA 1"},
        new Address { Detail = "Address of AAAA 2"}
    }
};


// Fake dữ liệu
unitOfWork.StudentRepository.AddNew(student);

Car car = new Car { Make = "Huy đẹp trai" };
unitOfWork.CarRepository.AddNew(car);
await unitOfWork.SaveChangesAsync(); // Lưu dữ liệu


Console.WriteLine("---------------");

List<Student> students = unitOfWork.StudentRepository.LoadStudentWithAddress();


List<Car> cars = unitOfWork.CarRepository.GetAll();
Console.WriteLine();
