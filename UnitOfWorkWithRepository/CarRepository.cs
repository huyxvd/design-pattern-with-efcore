﻿public interface ICarRepository : IGenericRepository<Car>
{
}

public class CarRepository : GenericRepository<Car>, ICarRepository
{
    public CarRepository(AppDbContext appContext) : base(appContext)
    {

    }
}