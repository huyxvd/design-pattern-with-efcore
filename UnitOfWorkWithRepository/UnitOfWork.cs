﻿public interface IUnitOfWork
{
    Task<int> SaveChangesAsync();

    public IAddressRepository AddressRepository { get; }

    public IStudentRepository StudentRepository { get; }
    public ICarRepository CarRepository { get; }
}

public class UnitOfWork : IUnitOfWork
{
    private readonly IStudentRepository _studentRepository;
    private readonly IAddressRepository _addressRepository;
    private readonly ICarRepository _carRepository;
    private readonly AppDbContext _dbContext;

    public UnitOfWork(AppDbContext dbContext)
    {
        _studentRepository = new StudentRepository(dbContext);
        _addressRepository = new AddressRepository(dbContext);
        _carRepository = new CarRepository(dbContext);
        _dbContext = dbContext;
    }
    public IAddressRepository AddressRepository => _addressRepository;
    public IStudentRepository StudentRepository => _studentRepository;
    public ICarRepository CarRepository => _carRepository;

    public Task<int> SaveChangesAsync() => _dbContext.SaveChangesAsync();
}

