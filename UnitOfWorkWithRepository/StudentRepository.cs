﻿using Microsoft.EntityFrameworkCore;

public interface IStudentRepository : IGenericRepository<Student>
{
    public List<Student> LoadStudentWithAddress();
}

public class StudentRepository : GenericRepository<Student>, IStudentRepository
{
    public StudentRepository(AppDbContext appContext) : base(appContext)
    {

    }
    public List<Student> LoadStudentWithAddress()
    {
        return _dbSet.Include(x => x.Addresss).ToList();
    }
}