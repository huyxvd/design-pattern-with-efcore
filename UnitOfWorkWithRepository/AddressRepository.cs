﻿using Microsoft.EntityFrameworkCore;

public interface IAddressRepository : IGenericRepository<Address>
{
    public List<Address> LoadAddressIncludeStudent();
}

public class AddressRepository : GenericRepository<Address>, IAddressRepository
{
    public AddressRepository(AppDbContext appContext) : base(appContext)
    {

    }

    public List<Address> LoadAddressIncludeStudent()
    {
        return _dbSet.Include(x => x.Student).ToList();
    }
}